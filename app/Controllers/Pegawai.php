<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\PegawaiModel;

class Pegawai extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;

    public function index()
    {
        $model = new PegawaiModel();
        $data = $model->findAll();
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => [
                'success' => 'Data Get'
            ],
            'data' => $data
        ];
        return $this->respond($response);
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        $model = new PegawaiModel();
        $data = $model->find(['id' => $id]);
        if(!$data) return $this->failNotFound('No Data Found');
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => [
                'success' => 'Data Get'
            ],
            'data' => $data[0]
        ];
        return $this->respond($response);
    }

    public function create(){
        // Validation
        $input = [
            'nama' => 'required',
            'alamat' => 'required',
            'file' => 'uploaded[file]|max_size[file,300]|ext_in[file,jpg,jpeg],'
        ];
        // $id = $this->request->getVar('id');
        $nama = $this->request->getVar('nama');
        $alamat = $this->request->getVar('alamat');

        if(!$this->validate($input)) return $this->fail($this->validator->getErrors());
        else{ // Valid
            if($file = $this->request->getFile('file')) {
                if ($file->isValid() && ! $file->hasMoved()) {
                    // Get file name and extension
                    $name = $file->getName();
                    $ext = $file->getClientExtension();

                    // Get random file name
                    $newName = $file->getRandomName(); 

                    // Store file in public/uploads/ folder
                    $file->move('../public/uploads', $newName);

                    // File path to display preview
                    $filepath = base_url()."/uploads/".$newName;
                    $data = ['nama_pegawai' => $nama, 'alamat_pegawai' => $alamat, 'foto_pegawai' => $filepath];
                    $model = new PegawaiModel();
                    $result = $model->save($data);
                    $response = [
                        'status' => 200,
                        'error' => null,
                        'messages' => [
                            'success' => 'Data Updated'
                        ]
                    ];
                    return $this->respond($response);
                }else{
                    $response = [
                        'status' => 500,
                        'error' => null,
                        'messages' => [
                            'fail' => 'Image not uploaded'
                        ]
                    ];
                    return $this->fail($response);
                }
              }

         }
  
         return redirect()->route('/'); 
    }

    public function update($id = null){
        // Validation
        $input = [
            'nama' => 'required',
            'alamat' => 'required',
        ];
        // $id = $this->request->getVar('id');
        $nama = $this->request->getVar('nama');
        $alamat = $this->request->getVar('alamat');

        if(!$this->validate($input)) return $this->fail($this->validator->getErrors());
        else{ // Valid
            if($file = $this->request->getFile('file')) {
                $rules = [
                    'file' => 'uploaded[file]|max_size[file,300]|ext_in[file,jpg,jpeg],'
                ];
                if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
                if ($file->isValid() && ! $file->hasMoved()) {
                    // Get file name and extension
                    $name = $file->getName();
                    $ext = $file->getClientExtension();

                    // Get random file name
                    $newName = $file->getRandomName(); 

                    // Store file in public/uploads/ folder
                    $file->move('../public/uploads', $newName);

                    // File path to display preview
                    $filepath = base_url()."/uploads/".$newName;
                    $data = ['nama_pegawai' => $nama, 'alamat_pegawai' => $alamat, 'foto_pegawai' => $filepath];
                    $model = new PegawaiModel();
                    $result = $model->update($id, $data);
                    $response = [
                        'status' => 200,
                        'error' => null,
                        'messages' => [
                            'success' => 'Data Updated'
                        ]
                    ];
                    return $this->respond($response);
                }else{
                    $response = [
                        'status' => 500,
                        'error' => null,
                        'messages' => [
                            'fail' => 'Image not uploaded'
                        ]
                    ];
                    return $this->fail($response);
                }
            }else{
                $data = ['nama_pegawai' => $nama, 'alamat_pegawai' => $alamat];
                $model = new PegawaiModel();
                $result = $model->update($id, $data);
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => [
                        'success' => 'Data Updated'
                    ]
                ];
                return $this->respond($response);
            }

         }
  
         // return redirect()->route('/'); 
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $model = new PegawaiModel();
        $data = $model->where('id', $id)->delete();
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => [
                'success' => 'Data deleted'
            ]
        ];
        return $this->respond($response);
    }
}
