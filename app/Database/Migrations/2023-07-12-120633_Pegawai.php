<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pegawai extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'           => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'nama_pegawai'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'alamat_pegawai'     => [
                'type'           => 'TEXT'
            ],
            'foto_pegawai'     => [
                'type'           => 'TEXT'
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('pegawai');
    }

    public function down()
    {
        //
    }
}
